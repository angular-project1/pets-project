import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PetsService } from '../pets/pets.service';
import { Validators } from '@angular/forms';
import {Router} from "@angular/router"
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  // private image:any;


petForm:any= new FormGroup({
  email: new FormControl('', [Validators.required, Validators.email, Validators.minLength(5)]),
  animal: new FormControl('', [Validators.required, Validators.minLength(1)]),
  name: new FormControl('', [Validators.required, Validators.minLength(1)]),
  age: new FormControl(''),
  description: new FormControl(''),
  img: new FormControl(''),
  protect: new FormControl(''),
  inreception: new FormControl('false'),
})

  constructor(private petsService: PetsService, private router: Router) { }

  ngOnInit(): void {
  }


  onSubmit() {
    this.petsService.addPet(this.petForm.value);
    this.petForm.reset();
    this.petsService.msg.next('Se ha subido correctamente!');
    this.router.navigate(['/pets'])
  }
   handleImage(event:any):void{
     const img = event.target.files[0];
     var reader = new FileReader();
     reader.onloadend = () =>{
       this.petForm.controls.img.setValue(reader.result);
     }
     reader.readAsDataURL(img);

   }
}
