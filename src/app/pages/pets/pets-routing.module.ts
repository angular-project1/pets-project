import { PetDetailComponent } from './components/pet-detail/pet-detail.component';
import { PetsComponent } from './pets.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: PetsComponent},
  { path: 'pet-detail/:id', component: PetDetailComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PetsRoutingModule { }
