import { SharedModule } from './../../shared/shared.module';
import { PetsService } from './pets.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PetsRoutingModule } from './pets-routing.module';
import { PetsComponent } from './pets.component';
import { PetDetailComponent } from './components/pet-detail/pet-detail.component';
import { FilterFormComponent } from './components/filter-form/filter-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PetsComponent, PetDetailComponent, FilterFormComponent],
  imports: [
    CommonModule,
    PetsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [PetsService],
})
export class PetsModule {}
