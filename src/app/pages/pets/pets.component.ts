import { Observable, Subscription } from 'rxjs';
import { PetsService } from './pets.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Pet } from './model/pet.model';

// import { LoaderComponent } from 'src/app/shared/loader/loader.component';

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.scss'],
})
export class PetsComponent implements OnInit, OnDestroy {
  // Se declara e inicia en el componente padre para luego comunicarlo al componente hijo
  // inputText: string = '';
  // Variable donde almacenamos el valor del hijo
  // sonMessage: string = '';
  // con cada tecla apretada se activa esta funcion.
  // keyUp(letra: string) {
  // this.inputText = letra;
  // }
  // Recibe el mensaje del hijo
  // setMessage(message: string): void {
  // this.sonMessage = message;
  // }
  public petFound: Pet | null = null;
  pets: Pet[] | undefined;
  pets_bk: Pet[] | undefined;
  isLoading: boolean = true;

  getPetsSubs: any;


/**
 * .subscribe((personas)=>{}, error=>{}, =>{})
 */

  constructor(private petsService: PetsService) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.getPetsSubs = this.petsService.getPets().subscribe((pets: Pet[]) => {
      this.pets = pets;
      this.pets_bk=pets;
      this.isLoading = false;
    });
  }
  ngOnDestroy(){
  }

  handlePet(animal: string) {
    this.pets=this.pets_bk?.filter(pet=>pet.animal.trim().toLowerCase().includes(animal.trim().toLowerCase()))
  }




}
// function animal(animal: any) {
//   throw new Error('Function not implemented.');
// }
