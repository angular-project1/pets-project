import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';
import { BehaviorSubject, Observable } from 'rxjs';
import { Pet } from './model/pet.model';
import { map } from 'rxjs/operators';


@Injectable()
export class PetsService {
  petsCollection: AngularFirestoreCollection<Pet> | any;
  petDoc: AngularFirestoreDocument<Pet> | any;
  pets: Observable<Pet[]> | any;
  pet: Observable<Pet> | any;

  private filePath: any;
  private downloadURL:Observable<string> | undefined;

  constructor(private db: AngularFirestore) {
    this.petsCollection = db.collection('pets', (ref) =>
      ref.orderBy('animal', 'asc')
    );
  }

  getPets(): Observable<Pet[]> {
    this.pets = this.petsCollection.snapshotChanges().pipe(
      map((cambios: { payload: { doc: { data: () => Pet; id: any } } }[]) => {
        return cambios.map(
          (accion: { payload: { doc: { data: () => Pet; id: any } } }) => {
            const datos: Pet = accion.payload.doc.data();
            datos.id = accion.payload.doc.id;
            return datos;
          }
        );
      })
    );
    return this.pets;
  }

  getPetDetail(id: string) {
    return this.db
      .collection('pets')
      .doc(id)
      .snapshotChanges()
      .pipe(
        map((pet) => {
          return pet.payload.data();
        })
      );
  }

  addPet(pet: Pet) {
    const ref = this.db.collection('pets');
    ref.add(pet);
  }

  private uploadImage(post: Pet, image:File){
    this.filePath = `images/${image.name}`

  }

  msg = new BehaviorSubject<any>('')
}
