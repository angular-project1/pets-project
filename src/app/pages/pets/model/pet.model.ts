export interface Pet {
  id?: any;
  age: number;
  description?:string;
  img?:string;
  inreception?:boolean | string;
  name:string;
  protect?:string;
  animal: string;
  email: string;
}
