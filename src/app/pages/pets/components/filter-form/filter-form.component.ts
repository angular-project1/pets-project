import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-filter-form',
  templateUrl: './filter-form.component.html',
  styleUrls: ['./filter-form.component.scss'],
})
export class FilterFormComponent implements OnInit {
  @Output() sendName = new EventEmitter<string>();
  // @Input() inputText: string | undefined;
  // @Output() emitMessage = new EventEmitter<string>();
  // message: string = '';
  // sendMessage() {
  // this.emitMessage.emit(this.message);
  // }
query='';
  public finderForm: FormGroup = new FormGroup({});

  // constructor(private router: Router) {}
  constructor() {}

  ngOnInit(): void {
    this.finderForm = new FormGroup({
      name: new FormControl('', []),
    });
  }

  onSubmit(): void {
     console.log(this.query);
    this.sendName.emit(this.query);
  }
  // onSearch(value:string){
  //   console.log('Buscar', value)
  //     if(value && value.length > 3){
  //       this.router.navigate(['/pet-detail/'], {
  //         queryParams:{q:value}
  //       })
  //     }
  // }
}
