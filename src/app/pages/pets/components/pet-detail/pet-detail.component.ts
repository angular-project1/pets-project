import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pet } from '../../model/pet.model';
import { PetsService } from '../../pets.service';

@Component({
  selector: 'app-pet-detail',
  templateUrl: './pet-detail.component.html',
  styleUrls: ['./pet-detail.component.scss']
})
export class PetDetailComponent implements OnInit {

  pet:Pet | undefined;
  test='test'
  constructor(private petsService: PetsService, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.route.queryParams
      .subscribe(params => {
        const id =this.route.snapshot.paramMap.get('id');
        if(id){
          this.getPetsDetail(id);
        }
      }
    );
  }
  getPetsDetail(petId: string) {
    this.petsService.getPetDetail(petId).subscribe((pet: Pet|any)=>{
      this.pet=pet;
      console.log(pet)
    })
  }


}
