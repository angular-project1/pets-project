import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../models/core.models';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  public menuList: MenuItem[] = [];

  constructor() { }

  ngOnInit(): void {
    this.menuList = [
      {
        label: 'Inicio',
        img: 'http://www.june.es/pets/casa.svg',
        url: '/home'
      },
      {
        label: 'Mascotas',
        img: 'http://www.june.es/pets/huella.svg',
        url: '/pets'
      },
      {
        label: 'Añadir',
        img: 'http://www.june.es/pets/anadir.svg',
        url: '/add'
      },
    ]
  }

}
