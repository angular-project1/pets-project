export interface MenuItem {
  label: string;
  img: string;
  url: string;
}
