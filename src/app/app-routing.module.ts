import { PetDetailComponent } from './pages/pets/components/pet-detail/pet-detail.component';
import { NotFoundComponent } from './core/components/not-found/not-found.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'pets',
    loadChildren: () =>
      import('./pages/pets/pets.module').then((m) => m.PetsModule),
  },
  {
    path: 'pet-detail/:id', component:PetDetailComponent
  },
  {
    path: 'add',
    loadChildren: () =>
      import('./pages/add/add.module').then((m) => m.AddModule),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: '**', component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
