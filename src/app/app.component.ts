import { Component } from '@angular/core';
import { PetsService } from './pages/pets/pets.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-project-pets';
  msg='';
  constructor(private petsService:PetsService, private _snackBar: MatSnackBar){
    this.petsService.msg.subscribe((msg)=>{
    this.msg=msg;
    if(msg !== ''){
    this.openSnackBar()}
    })
  }
  openSnackBar() {
    this._snackBar.open(this.msg, 'cerrar');
  }


}
