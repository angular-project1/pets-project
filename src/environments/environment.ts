// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firestore:{
    apiKey: "AIzaSyD1ArrobSPR2NSli76UoNj6a0DTI8YqD0I",
  authDomain: "pets-32e24.firebaseapp.com",
  projectId: "pets-32e24",
  storageBucket: "pets-32e24.appspot.com",
  messagingSenderId: "694746386622",
  appId: "1:694746386622:web:55b3be75121510d65a8eef",
  measurementId: "G-V50RWXLKXS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
